const StatusCollection = new Mongo.Collection('status');

export function isReady(option) {
    const status = StatusCollection.findOne(option);
    if (status) {
        return true;
    }
    return false;
}

export function ready(option) {
    if (!isReady(option)) {
        StatusCollection.insert(option);
    }
}