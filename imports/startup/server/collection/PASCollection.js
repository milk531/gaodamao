import {
    Mongo
} from 'meteor/mongo';
import {
    Meteor,
} from 'meteor/meteor';
import {
    exec,
} from 'child_process';

import {
    addJob,
    processJobs,
    getJob,
} from '../jobQueue.js';
import {
    isReady,
    ready,
} from './StatusCollectioin.js'


const TradeCollection = new Mongo.Collection('trade');
const TicksCollection = new Mongo.Collection('ticks');
const WinnerCollection = new Mongo.Collection('winner');

const NormalCollection = new Mongo.Collection('znz_pas_normal');
const AbnormalCollection = new Mongo.Collection('znz_pas_abnormal');

Meteor.publish('znz_pas_normal', () => NormalCollection.find());
Meteor.publish('znz_pas_abnormal', () => AbnormalCollection.find());

export function addPASSchedule() {
    SyncedCron.add({
        name: 'Trade Data',
        schedule: function (parser) {
            return parser.cron('10 18 * * *');
        },
        job: function () {
            pullTrade();
        }
    });
}
export function pullTrade() {
    const dateStr = moment(new Date()).format("YYYY-MM-DD");
    if (!isReady({
            type: 'trade',
            date: dateStr
        })) {
        exec('python assets/app/cmd/trade.py', Meteor.bindEnvironment(function (err, stdout, stderr) {
            if (err) {
                console.info('Pull Trade Failed:' + err);
            } else {
                console.info('Pull Trade Success!');
                ready({
                    type: 'trade',
                    date: dateStr
                });
                createJobQueue(dateStr);
                processJobs('pullTick', doJob);
            }
        }));
    } else {
        createJobQueue(dateStr);
        processJobs('pullTick', doJob);
    }
}
export function updateZNZ() {
    let days = 0;
    let dateStr;
    let tradeToday;
    while (days < 15) {
        dateStr = moment().add(-days, 'days').format("YYYY-MM-DD");
        tradeToday = TradeCollection.find({
            date: dateStr
        });
        if (tradeToday.count() != 0) {
            break;
        }
        days++;
    }
    NormalCollection.remove({});
    AbnormalCollection.remove({});
    tradeToday.forEach((trade) => {
        let count = 0;
        let normal = false;
        days = 0;
        while (count < 5) {
            const tempDate = moment(dateStr, "YYYY-MM-DD").add(-days, 'days').format("YYYY-MM-DD");
            const tempWin = WinnerCollection.findOne({
                code: trade.code,
                date: tempDate
            });
            if (tempWin) {
                console.info(tempWin.rop);
                if (Math.abs(tempWin.rop) > 16) {
                    normal = true;
                    break;
                }
                count++;
            }
            days++;
            if (days > 15)
                break;
        }
        if (normal) {
            NormalCollection.insert({
                code: trade.code,
                date: dateStr
            });
        } else {
            AbnormalCollection.insert({
                code: trade.code,
                date: dateStr
            });
        }
    });
}

function doJob(job, cb) {
    const data = job.data;
    console.info(`Pull Ticks ${data.code} on ${data.date}`);
    if (!isReady({
            type: 'tick',
            date: data.date,
            code: data.code
        })) {
        exec('python assets/app/cmd/ticks.py -c ' + data.code + ' -d ' + data.date, Meteor.bindEnvironment(function (err, stdout, stderr) {
            if (err) {
                if (err.code == 10) {
                    job.done();
                } else {
                    job.fail('Pull Ticks for ' + data.code + ' fail:' + err);
                }
            } else {
                ready({
                    type: 'tick',
                    date: data.date,
                    code: data.code
                });
                let allVol = 0;
                let winVol = 0;
                TicksCollection.find({
                    date: data.date,
                    code: data.code,
                    type: '买盘',
                }).forEach((ticket) => {
                    allVol += ticket.volume;
                    if (ticket.price < data.close) {
                        winVol += ticket.volume;
                    }
                });
                const winner = winVol / allVol * 100;
                const rop = getROP(data.code, winner, data.date, data.a);
                if (rop != undefined)
                    WinnerCollection.insert({
                        code: data.code,
                        date: data.date,
                        winner: winner,
                        rop: rop
                    });
                else
                    WinnerCollection.insert({
                        code: data.code,
                        date: data.date,
                        winner: winner
                    });
                job.done();
            }
            cb();
        }));
    } else {
        job.done();
        cb();
    }
}

function getROP(code, winner, dateStr, a) {
    const lastDateStr = moment(dateStr, "YYYY-MM-DD").add(-1, 'days').format("YYYY-MM-DD");
    const winB = WinnerCollection.findOne({
        code: code,
        date: lastDateStr
    });
    if (winB) {
        const b = winner - winB.winner;
        return b / a;
    }
    return undefined;
}

function createJobQueue(dateStr) {
    TradeCollection.find({
        date: dateStr
    }).forEach((trade) => {
        if (!getJob({
                type: 'pullTick',
                'data.code': trade.code,
                'data.date': dateStr
            })) {
            addJob('pullTick', {
                code: trade.code,
                close: trade.trade,
                date: dateStr,
                a: trade.turnoverratio,
            });
        }
    });
}