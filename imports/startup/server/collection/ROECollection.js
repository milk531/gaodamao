import {
    Mongo
} from 'meteor/mongo';
import {
    Meteor,
} from 'meteor/meteor';
import {
    exec,
} from 'child_process';
import {
    Stat
} from '../stat.js';
import Fiber from 'fibers';

const ROETopCollection = new Mongo.Collection('ROETop');
const ROESettingCollection = new Mongo.Collection('ROESetting');

Meteor.publish('roeTops', () => ROETopCollection.find());

let ROECollection = {};
let settingUpdated = false;

export function updateROESetting() {
    const setting = ROESettingCollection.findOne();
    if (setting) {
        if (setting.period != Meteor.settings.roe.period) {
            settingUpdated = true;
            ROESettingCollection.update(setting._id, {
                $set: {
                    'period': Meteor.settings.roe.period
                }
            });
        }
        if (setting.minimum != Meteor.settings.roe.minimum) {
            settingUpdated = true;
            ROESettingCollection.update(setting._id, {
                $set: {
                    'minimum': Meteor.settings.roe.minimum
                }
            });
        }
        if (setting.top != Meteor.settings.roe.top) {
            settingUpdated = true;
            ROESettingCollection.update(setting._id, {
                $set: {
                    'top': Meteor.settings.roe.top
                }
            });
        }
    } else {
        settingUpdated = true;
        ROESettingCollection.insert({
            period: Meteor.settings.roe.period,
            minimum: Meteor.settings.roe.minimum,
            top: Meteor.settings.roe.top
        });
    }
}





export function addROESchedule() {
    SyncedCron.add({
        name: 'Profit Data',
        schedule: function (parser) {
            return parser.cron('59 23 28 1,2,3,4 *');
        },
        job: function () {
            const year = now.getFullYear() - 1;
            if (!isPulled(year)) {
                pull(year, function (error, stdout, stderr) {
                    if (error) {
                        console.error('Pull profit data faild:' + error);
                    } else {
                        console.info('Pull profit data success:' + stdout);
                        updateCollection(getYearQueue());
                    }
                });
            }
        }
    });
}

export function pullROE() {
    const allYear = getYearQueue();
    const yearQueue = [];
    for (let i = 0; i < allYear.length; i++) {
        if (!isPulled(allYear[i])) {
            yearQueue.push(allYear[i]);
        }
    }
    const totalCount = yearQueue.length;
    PullQueue(0, yearQueue, totalCount);
}

function updateCollection(yearQueue) {
    const newCollection = {};
    for (let i = 0; i < yearQueue.length; i++) {
        const year = yearQueue[i];
        if (ROECollection[year]) {
            newCollection[year] = ROECollection[year];
        } else {
            newCollection[year] = new Mongo.Collection('profit_' + year);
        }
    }
    ROECollection = newCollection;
}

function PullQueue(index, yearQueue, totalCount) {
    if (index < yearQueue.length) {
        let y = yearQueue[index];
        pull(y, function () {
            Stat.progress = (index + 1) / totalCount;
            PullQueue(index + 1, yearQueue, totalCount);
        });
    } else {
        Stat.progress = 1;
        updateCollection(getYearQueue());
        Fiber(function () {
            ROETop();
        }).run();
    }
}

function isPulled(year) {
    const profit = new Mongo.Collection('profit_' + year);
    ROECollection[year] = profit;
    if (profit.find().count() != 0)
        return true;
    return false;
}

function pull(year, callback) {
    exec('python assets/app/cmd/profit.py -y ' + year + ' -q 4', callback);
}

function getYearQueue() {
    const now = new Date();
    const year = now.getFullYear();
    const start_year = year - Meteor.settings.roe.period;

    const yearQueue = [];
    for (let i = start_year; i < year; i++) {
        yearQueue.push(i);
    }
    return yearQueue;
}

function ROETop() {
    if (!needUpdate())
        return;
    const restYear = [];
    let basicList;
    let basicYear;
    let count = 0;
    let roeList = [];
    let topList = [];
    for (let year in ROECollection) {
        if (count == 0) {
            const profits = ROECollection[year];
            basicList = profits.find({
                roe: {
                    $gt: Meteor.settings.roe.minimum
                }
            });
            basicYear = year;
        } else {
            restYear.push(year);
        }
        count++;
    }

    basicList.forEach((p) => {
        let len = roeList.push([{
            year: basicYear,
            roe: p.roe,
        }]);
        topList.push({
            code: p.code,
            name: p.name,
            roeYearly: roeList[len - 1],
        });
        for (let i = 0; i < restYear.length; i++) {
            const year = restYear[i];
            const profits = ROECollection[year];
            const stocks = profits.find({
                roe: {
                    $gt: Meteor.settings.roe.minimum
                },
                code: {
                    $eq: p.code
                }
            }).fetch();
            if (stocks.length > 0) {
                roeList[len - 1].push({
                    year: year,
                    roe: stocks[0].roe,
                });
            }
        }
    });

    for (let i = topList.length - 1; i >= 0; i--) {
        const top = topList[i];
        if (top.roeYearly.length == restYear.length + 1) {
            let roeSum = 0;
            for (let j = 0; j < top.roeYearly.length; j++) {
                roeSum += top.roeYearly[j].roe;
            }
            top.avgROE = (roeSum / top.roeYearly.length).toFixed(2);
        } else {
            topList[i] = topList[topList.length - 1];
            topList.length = topList.length - 1;
        }
    }
    topList.sort((a, b) => {
        if (a.avgROE > b.avgROE)
            return -1;
        else if (a.avgROE < b.avgROE)
            return 1;
        else
            return 0;
    });
    topList = topList.slice(0, Meteor.settings.roe.top);
    ROETopCollection.remove({});
    const ROETopRaw = ROETopCollection.rawCollection();
    const bulk = ROETopRaw.initializeUnorderedBulkOp();
    topList.forEach((item) => bulk.insert(item));
    bulk.execute();
}

function needUpdate() {
    if (ROETopCollection.find().count() != Meteor.settings.roe.top ||
        settingUpdated)
        return true;
    return false;
}