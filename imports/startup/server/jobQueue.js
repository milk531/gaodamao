const TicksJobs = JobCollection('TicksJobsQueue');

export function startJobServer() {
    TicksJobs.startJobServer();
    TicksJobs.events.on('jobDone', function (msg) {
        if (!msg.error) {
            // TicksJobs.removeJobs([msg.params[0]]);
        }
    });
}

export function processJobs(jobType,processFun) {
    TicksJobs.processJobs(jobType,processFun);
}

export function addJob(jobType, jobData, until) {
    if (!until)
        until = Job.foreverDate;
    const job = new Job(TicksJobs, jobType, jobData);
    job.priority('normal').retry({
            retries: 5,
            until: until,
            wait: 10 * 60 * 1000
        })
        .save();
}
export function getJob(option){
   return TicksJobs.findOne(option);
}