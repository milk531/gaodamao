import {
  Meteor,
} from 'meteor/meteor';
import {
  updateROESetting,
  addROESchedule,
  pullROE,
} from './collection/ROECollection.js';
import {
  pullTrade,
  updateZNZ,
  addPASSchedule,
} from './collection/PASCollection.js';

import {
  startJobServer,
} from './jobQueue.js';
import {
  Stat
} from './stat.js';

Meteor.startup(() => {
  Stat.progress = 0.0;
  updateROESetting();
  addROESchedule();
  addPASSchedule();
  pullROE();
  pullTrade();
  SyncedCron.start();
  startJobServer();
});

Meteor.methods({
  updateZNZ: function () {
    updateZNZ();
  },
});