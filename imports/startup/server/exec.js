import {
    exec,
} from 'child_process';

import Future from 'fibers/future';

export function Exec(command){
    const future=new Future();
    exec(command,function(error,stdout,stderr){
      if(error){        
        future.throw(error);
      }else{
        future.return(stdout.toString());
      }
    });
    return future.wait();
}