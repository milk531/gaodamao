#!/usr/bin/python
#-*-coding:utf-8 -*-
import tushare as ts
import datetime
import time
import sys, getopt
from tushare.util import dateu as du
from pymongo import MongoClient
import json

def main(argv):
    code = ''
    date = '' 
    try:
        opts, args = getopt.getopt(argv,"hc:d:")
    except getopt.GetoptError:
        print('ticks.py -c code -d date')
        sys.exit(2)
    if len(opts)!=2:
        print('ticks.py -c code -d date')
        sys.exit(2)
    
    try:
        for opt, arg in opts:
            if opt == '-h':
                print('ticks.py -c code -d date')
                sys.exit()
            elif opt in ("-c"):
                int(arg)  
                code = arg
            elif opt in ("-d"):                
                date = arg
    except ValueError:
        print('Please give a valid param')
        sys.exit(2)    
    # # # Input
    # code：股票代码，即6位数字代码
    # retry_count : int, 默认3,如遇网络等问题重复执行的次数
    # pause : int, 默认 0,重复请求数据过程中暂停的秒数，防止请求间隔时间太短出现的问题
    # # # Output
    # time：时间
    # price：当前价格
    # pchange:涨跌幅
    # change：价格变动
    # volume：成交手
    # amount：成交金额(元)
    # type：买卖类型【买盘、卖盘、中性盘】
    # df = ts.get_today_ticks(code,3,5)
    if not du.is_holiday(date):
        df = ts.get_tick_data(code,date=date,retry_count=2,pause=3)    
        client = MongoClient('mongodb://localhost:3001/')
        db = client['meteor']
        ticks = db['ticks']
        ticksList = json.loads(df.to_json(orient='records'))
        for tick in ticksList:
            tick['date']=date
            tick['code']=code
        ticks.insert_many(ticksList)

    else:
        sys.exit(10)

if __name__ == "__main__":
    main(sys.argv[1:])
