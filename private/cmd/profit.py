#!/usr/bin/python
#-*-coding:utf-8 -*-
import tushare as ts
import sys, getopt
import json
from pymongo import MongoClient
#################################
# # # # # Input
# year
# quarter
# # # # # Output
# code
# name
# roe
# net_profit_ratio
# gross_profit_rate
# net_profits
# esp
# business_income
# bips
##################################
def main(argv):
    year = 0
    quarter = 0
    try:
        opts, args = getopt.getopt(argv,"hy:q:")
    except getopt.GetoptError:
        print('profit.py -y year -q quarter')
        sys.exit(2)
    if len(opts)!=2:
        print('profit.py -y year -q quarter')
        sys.exit(2)
    try:
        for opt, arg in opts:
            if opt == '-h':
                print('profit.py -y year -q quarter')
                sys.exit()
            elif opt in ("-y"):
                year = int(arg)  
                if year < 1990:
                    print('%s is not a valid year value' % arg)
                    sys.exit(2)              
            elif opt in ("-q"):
                quarter = int(arg)
                if quarter < 1 or quarter > 4:
                    print('%s is not a valid quarter value' % arg)
                    sys.exit(2)
    except ValueError:
        print('Please give a valid year or quarter')
        sys.exit(2)    

    pd=ts.get_profit_data(year,quarter)
    client = MongoClient('mongodb://localhost:3001/')
    db = client['meteor']
    profit = db['profit_%s' % (year)]
    profit.insert_many(json.loads(pd.to_json(orient='records')))

if __name__ == "__main__":
    main(sys.argv[1:])
