#!/usr/bin/python
#-*-coding:utf-8 -*-
import time
import json
import tushare as ts
import sys
from tushare.util import dateu as du
from pymongo import MongoClient

def main():
    # # # Input
    # None
    # # # Output
    # code:代码
    # name:名称
    # changepercent:涨跌幅
    # trade:现价
    # open:开盘价
    # high:最高价
    # low:最低价
    # settlement:昨日收盘价
    # volume:成交量
    # turnoverratio:换手率
    # amount:成交量
    # per:市盈率
    # pb:市净率
    # mktcap:总市值
    # nmc:流通市值
    if not du.is_holiday(du.today()):
        df=ts.get_today_all().head(50)
        client = MongoClient('mongodb://localhost:3001/')
        db = client['meteor']
        trade = db['trade']
        tradeList = json.loads(df.to_json(orient='records'))
        for stock in tradeList:
            stock['date']=time.strftime("%Y-%m-%d", time.localtime()) 
        trade.insert_many(tradeList)
    else:
        sys.exit(10)

if __name__ == "__main__":
    main()
