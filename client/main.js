import {
  Template
} from 'meteor/templating';
import {
  ReactiveVar
} from 'meteor/reactive-var';

import './main.html';

import {
  Meteor
} from 'meteor/meteor';

import {
  Session
} from 'meteor/session'

const ROETopCollection = new Mongo.Collection('ROETop');

const NormalCollection = new Mongo.Collection('znz_pas_normal');
const AbnormalCollection = new Mongo.Collection('znz_pas_abnormal');

let progressTimer;
let progressBar;
Meteor.startup(() => {
  Session.set('isLoading', true);
  Session.set('stateIndex',0);
  progressTimer = Meteor.setInterval(function () {
    Meteor.call('progress', function (err, response) {
      if (progressBar) {
        progressBar.animate(parseFloat(response));
        if (progressBar.value() >= 0.9965) {
          Meteor.clearInterval(progressTimer);
          Session.set('isLoading', false);
        }
      }
    });
  }, 200);
});

Template.main.onCreated(function () {
  this.subscribe('roeTops');
  this.subscribe('znz_pas_normal');
  this.subscribe('znz_pas_abnormal');
  Meteor.call('updateZNZ', function (err, response) {
    
  });
});

Template.loading.onRendered(function () {
  if (!progressBar) {
    progressBar = new ProgressBar.Circle('#loading_container', {
      color: '#aaa',
      strokeWidth: 4,
      trailWidth: 1,
      duration: 500,
      text: {
        autoStyleContainer: false
      },
      from: {
        color: '#aaa',
        width: 1
      },
      to: {
        color: '#00FF7F',
        width: 4
      },
      // Set default step function for all animate calls
      step: function (state, circle) {
        circle.path.setAttribute('stroke', state.color);
        circle.path.setAttribute('stroke-width', state.width);

        var value = Math.round(circle.value() * 100);
        if (value === 0) {
          circle.setText('');
        } else {
          circle.setText(value);
        }

      }
    });
    progressBar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
    progressBar.text.style.fontSize = '2rem';
    progressBar.animate(0);
  }
});
Template.body.helpers({
  isLoading() {
    return Session.get('isLoading');
  },
});
Template.main.helpers({
  roeTops() {
    return ROETopCollection.find();
  },
  state(index){
    if(index == Session.get('stateIndex')){
      return true;
    }
    return false;
  },
  normalList(){
    return NormalCollection.find();
  },
  abnormalList(){
    return AbnormalCollection.find();
  },
});

Template.main.events({
  'click #ROETOP' (event, templateInstance) {
    Session.set('stateIndex',0);
  },
  'click #ZNZ_PAS' (event, templateInstance) {
    Session.set('stateIndex',1);
  },
});